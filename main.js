
(function () {
    'use strict';

    const hamburguer = document.querySelector('#hamburguer');    
    const navBar = document.querySelector('#navbar');
    

    hamburguer.addEventListener("click", () => {
      if (navBar.classList.contains('show_nav')) {
          navBar.classList.remove('show_nav');
          navBar.classList.add('hide_nav');

      } else {
        navBar.classList.remove('hide_nav');
        navBar.classList.add('show_nav');
      }
    });


})()
    




